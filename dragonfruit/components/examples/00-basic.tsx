import React from 'react';

import ComponentList from '../src';

export default function Basic() {
  return <ComponentList testId="component-list" />;
}
