# Components

This package contains common components for compass "Components".


## Usage

`import ComponentList from '@atlaskit/components';`

Detailed docs and example usage can be found [here](https://atlaskit.atlassian.com/packages/dragonfruit/components).
