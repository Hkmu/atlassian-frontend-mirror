export const INLINE_UNSUPPORTED_CONTENT_TEXT_ATTR_VALUE =
  'Invalid inline node content';

//============Unsupported Content Types===========
export const BLOCK_UNSUPPORTED_CONTENT = 'block';
export const INLINE_UNSUPPORTED_CONTENT = 'inline';

//============Color Code======================
export const GREEN_RGB_CODE = 'rgb(0, 255, 0)';
export const GREEN_HEX_CODE = '#00FF00';
