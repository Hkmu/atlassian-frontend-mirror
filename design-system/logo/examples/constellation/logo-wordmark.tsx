import React from 'react';

import { B400 } from '@atlaskit/theme/colors';

import { AtlassianWordmark } from '../../src';

export default () => <AtlassianWordmark textColor={B400} />;
