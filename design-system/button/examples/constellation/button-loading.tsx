import React from 'react';

import { LoadingButton } from '../../src';

export default () => (
  <LoadingButton appearance="primary" isLoading>
    Loading button
  </LoadingButton>
);
