export { default } from './components/Icon';
export { default as SVG } from './components/SVG';
export { sizeMap as size, sizes } from './constants';
export { default as Skeleton } from './components/Skeleton';

export type {
  Size,
  SkeletonProps,
  IconProps,
  GlyphProps,
  CustomGlyphProps,
  SVGProps,
} from './types';
