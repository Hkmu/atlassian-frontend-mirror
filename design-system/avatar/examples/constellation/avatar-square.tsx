import React from 'react';

import Avatar from '../../src';

export default function AvatarSquareExample() {
  return (
    <Avatar
      appearance="square"
      size="medium"
      src="https://hello.atlassian.net/secure/projectavatar?pid=30630"
    />
  );
}
