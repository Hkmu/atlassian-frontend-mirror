import React from 'react';

import { Status } from '../../src';

export default function AvatarStatusApprovedExample() {
  return (
    <div style={{ width: 24 }}>
      <Status status="approved" />
    </div>
  );
}
