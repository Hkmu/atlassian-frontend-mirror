import React from 'react';

import { Status } from '../../src';

export default function AvatarStatusDeclinedExample() {
  return (
    <div style={{ width: 24 }}>
      <Status status="declined" />
    </div>
  );
}
