import { CSSObject } from '@emotion/core';

export const ellipsisStyle: CSSObject = {
  display: 'inline-flex',
  textAlign: 'center',
  alignItems: 'center',
  padding: '0 8px',
};

export const navStyle: CSSObject = {
  display: 'flex',
};
