export { default as Code, getCodeStyles } from './code';
export { default as CodeBlock } from './code-block';

export type {
  CodeBlockProps,
  CodeProps,
  SupportedLanguages,
  LanguageAlias,
} from './types';

export { SUPPORTED_LANGUAGES } from './constants';
