import React from 'react';

import Badge from '../src';

export default () => {
  return <Badge appearance="important">{25}</Badge>;
};
